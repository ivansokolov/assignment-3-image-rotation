#ifndef BMP_H
#define BMP_H

#include "img.h"
#include <stdio.h>

#define STATUS_ENUM_LIST(X) \
	X(BMP_LOAD_OK, "OK"), \
	X(BMP_LOAD_BAD_STREAM, "Bad input stream: failed to read header"), \
	X(BMP_LOAD_UNSUPPORTED_HEADER, "Bad or unsupported values in BMP header"), \
	X(BMP_LOAD_BAD_PIXELSTREAM, "Bad pixel stream: failed to read all pixels ")
STATUS_ENUM_GENERATE(BMP_load_status, bmp_load_messages);
#undef STATUS_ENUM_LIST

/*
 * `image` must not already be a valid image.
 * After a successful call (BMP_LOAD_OK), it becomes one
 */
BMP_load_status bmp_load_image (Image* img, FILE* stream);


#define STATUS_ENUM_LIST(X) \
	X(BMP_SAVE_OK, "OK"), \
	X(BMP_SAVE_WRITE_FAILED, \
			"Failed to write complete BMP image: stream may have partial data")
STATUS_ENUM_GENERATE(BMP_save_status, bmp_save_messages);
#undef STATUS_ENUM_LIST

/* Save an image view into a BMP file */
BMP_save_status bmp_save_view (const Image_view* imv, FILE* stream);

/* Shorthand for saving whole image */
BMP_save_status bmp_save_image (const Image* img, FILE* stream);

#endif /* BMP_H */
