#ifndef UTIL_H
#define UTIL_H

#include <assert.h>
#include <stddef.h>

#define ALIGNOF(X) _Alignof(X)

#define UNUSED(X) ((void) (X))

#define PACKED __attribute__((packed))
#define ALIGNED(AMT) __attribute__((aligned(AMT)))
#define NORETURN _Noreturn
#define LIKE_PRINTF(NUM_ARGS) __attribute__((format(printf, NUM_ARGS, (NUM_ARGS)+1)))

/* Similar to `inline` in C++, in principle allows to define symbols in headers */
#define HEADERDEF __attribute__((weak))

#define LIKELY(X) __builtin_expect(!!(X), 1)
#define UNLIKELY(X) __builtin_expect(!!(X), 0)
#define ASSUME(X) do { if (!(X)) __builtin_unreachable(); } while (0)

NORETURN void LIKE_PRINTF(1) fatal (const char* fmt, ...);
void LIKE_PRINTF(1) info (const char* fmt, ...);

#ifdef NO_MEMCPY_SUPERSTITION
#include <string.h>
#define MEMCPY memcpy
#else
#define MEMCPY memcpy_but_worse
void* memcpy_but_worse (void* dest, const void* src, size_t amount);
#endif

/*
 * X-macro-style utility to generate both a enum and an associated array of strings.
 *
 *    // Use site in header file
 *    #define STATUS_ENUM_LIST(X) \
 *      X(STATUS_ok, "OK"), \
 *      X(STATUS_frobnication_failed, "Failed to frobnicate"), \
 *      X(STATUS_interrupted, "Interruption ocurred")
 *    STATUS_ENUM_GENERATE(Frobnication_status, frob_status_messages);
 *    #undef STATUS_ENUM_LIST
 *
 * Commas and semicolons are at use site, to keep auto-formatters sane.
 *
 *    // Callsite
 *    Frobnication_status status = ...;
 *    if (status != STATUS_ok)
 *      fputs(frob_status_messages[status]);
 */
#define STATUS_ENUM_XMACRO_IN_ENUM_(ID,MESSAGE) ID
#define STATUS_ENUM_XMACRO_IN_ARRAY_(ID,MESSAGE) [ID]=MESSAGE
#define STATUS_ENUM_GENERATE(ENUM_NAME,MSG_ARR_NAME) \
	typedef enum ENUM_NAME { /* NOLINT: macro argument used as name for declaration */ \
	                         /* (in other places too, but those permit parentheses) */ \
		STATUS_ENUM_LIST(STATUS_ENUM_XMACRO_IN_ENUM_) \
	} (ENUM_NAME); \
	HEADERDEF const char* const (MSG_ARR_NAME)[] = { \
		STATUS_ENUM_LIST(STATUS_ENUM_XMACRO_IN_ARRAY_) \
	}

void* malloc_or_die (size_t size);
void* calloc_or_die (size_t nmemb, size_t size);
void* realloc_or_die (void* ptr, size_t size);

#endif /* UTIL_H */
