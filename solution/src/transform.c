#include "transform.h"
#include <stdbool.h>

static bool dimension_fit (const Image_view* dest, const Image_view* src)
{
	return dest->width >= src->width && dest->height >= src->height;
}

static bool dimension_fit_transposed (const Image_view* dest, const Image_view* src)
{
	return dest->height >= src->width && dest->width >= src->height;
}


void imv_blit (const Image_view* restrict dest, const Image_view* restrict src)
{
	assert(dimension_fit(dest, src));

	if (dest->width == src->width
	&& dest->stride == dest->width
	&& src->stride == src->width) {
		// Can do with just one copy in this case
		MEMCPY(dest->begin, src->begin, src->width * src->height * sizeof(RGB_pixel));
		return;
	}
	// Else there are gaps in either view, copy one row at a time
	RGB_pixel* dp = dest->begin;
	const RGB_pixel* sp = src->begin;
	for (size_t row = 0; row < src->height; row++) {
		MEMCPY(dp, sp, src->width * sizeof(RGB_pixel));
		dp += dest->stride;
		sp += src->stride;
	}
}


/*
 * The general "blit with linear transform" function. Always fills the destination view
 * completely: it is the caller's responsibility to ensure that the size of the source
 * pixel data and the transform parameters are consistent.
 *
 * `dest` view is always traversed left->right, top->bottom, starting at top left
 * `src` points into an image's pixel data and is traversed linearly, according to:
 * `src_pix_shift`, the distance between adjacent pixels within a row
 * `src_row_shift`, the distance between the beginning of adjacent rows
 *
 * If we want either pix or row shifts to jump across source rows, we need
 * to take stride into account when constructing `src_pix_shift` and `src_row_shift`
 *
 * Examples (`dest` and `src` are `Image_view`s)
 *   imv_blit_transform(dest, src->begin, 1, src->stride);   // naive blit
 *   imv_blit_transform(dest, src->begin, src->stride, 1);   // transpose
 *   imv_blit_transform(dest, src->begin, 2, 2*src->stride); // lossy downscale
 *   imv_blit_transform(dest, src->begin, 0, 0); // fill `dest` with top-left pixel of `src`
 */
static void imv_blit_transform
(const Image_view* dest, const RGB_pixel* src, long src_pix_shift, long src_row_shift)
{
	assert(dest);

	for (size_t row = 0; row < dest->height; row++) {
		RGB_pixel* cur_dest = dest->begin + dest->stride * row;
		const RGB_pixel* cur_src = src;
		for (size_t pix = 0; pix < dest->width; pix++) {
			*cur_dest++ = *cur_src;
			cur_src += src_pix_shift;
		}
		src += src_row_shift;
	}
}


void imv_rotate (Rotate_direction dir,
		const Image_view* restrict dest,
		const Image_view* restrict src)
{
	switch (dir) {
	case ROTATE_CW:
		assert(dimension_fit_transposed(dest, src));
		imv_blit_transform(dest,
				src->begin + IMGVIEW_PIXEL_OFFS(src, src->width-1, 0),
				(long) src->stride, -1);
		break;
	case ROTATE_CCW:
		assert(dimension_fit_transposed(dest, src));
		imv_blit_transform(dest,
				src->begin + IMGVIEW_PIXEL_OFFS(src, 0, src->height-1),
				-(long) src->stride, 1);
		break;
	case ROTATE_180:
		assert(dimension_fit(dest, src));
		imv_blit_transform(dest,
				src->begin + IMGVIEW_PIXEL_OFFS(src, src->width-1, src->height-1),
				-1, -(long) src->stride);
		break;
	}
}

void imv_mirror (Mirror_direction dir,
		const Image_view* restrict dest,
		const Image_view* restrict src)
{
	assert(dimension_fit(dest, src));
	switch (dir) {
	case MIRROR_ALONG_X:
		imv_blit_transform(dest,
				src->begin + IMGVIEW_PIXEL_OFFS(src, src->width-1, 0),
				-1, (long) src->stride);
		break;
	case MIRROR_ALONG_Y:
		imv_blit_transform(dest,
				src->begin + IMGVIEW_PIXEL_OFFS(src, 0, src->height-1),
				1, -(long) src->stride);
		break;
	}
}

void imv_transpose (Transpose_diagonal diag,
		const Image_view* restrict dest,
		const Image_view* restrict src)
{
	assert(dimension_fit_transposed(dest, src));
	switch (diag) {
	case MAIN_DIAGONAL:
		imv_blit_transform(dest, src->begin, (long) src->stride, 1);
		break;
	case SECONDARY_DIAGONAL:
		imv_blit_transform(dest,
				src->begin + IMGVIEW_PIXEL_OFFS(src, src->width-1, src->height-1),
				-(long) src->stride, -1);
		break;
	}
}


void img_create_cloned (Image* dest, const Image* src)
{
	img_create_uninitialized(dest, src->width, src->height);
	const Image_view vd = imv_from_image(dest);
	const Image_view vs = imv_from_image(src);
	imv_blit(&vd, &vs);
}

void img_create_rotated (Rotate_direction dir, Image* dest, const Image* src)
{
	size_t new_width = -1, new_height = -1;
	switch (dir) {
	case ROTATE_CW:
	case ROTATE_CCW:
		new_width = src->height;
		new_height = src->width;
		break;
	case ROTATE_180:
		new_width = src->width;
		new_height = src->height;
		break;
	}

	img_create_uninitialized(dest, new_width, new_height);
	const Image_view vd = imv_from_image(dest);
	const Image_view vs = imv_from_image(src);
	imv_rotate(dir, &vd, &vs);
}

void img_create_mirrored (Mirror_direction dir, Image* dest, const Image* src)
{
	img_create_uninitialized(dest, src->width, src->height);
	const Image_view vd = imv_from_image(dest);
	const Image_view vs = imv_from_image(src);
	imv_mirror(dir, &vd, &vs);
}

void img_create_transposed (Transpose_diagonal diag, Image* dest, const Image* src)
{
	img_create_uninitialized(dest, src->height, src->width); // Transposed dimensions!
	const Image_view vd = imv_from_image(dest);
	const Image_view vs = imv_from_image(src);
	imv_transpose(diag, &vd, &vs);
}
