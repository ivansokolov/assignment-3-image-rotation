#include "img.h"
#include "util.h"
#include <stdint.h>
#include <stdlib.h>

void img_create_zeroed (Image* img, size_t width, size_t height)
{
	img->width = width;
	img->height = height;
	img->pixels = calloc_or_die(width * height, sizeof(RGB_pixel));
}

void img_create_uninitialized (Image* img, size_t width, size_t height)
{
	img->width = width;
	img->height = height;
	img->pixels = malloc_or_die(width * height * sizeof(RGB_pixel));
}

void img_destroy (Image* img)
{
	free(img->pixels);
	img->pixels = NULL;
	img->width = 0;
	img->height = 0;
}


Image_view imv_from_image (const Image* src)
{
	Image_view result;
	result.begin = src->pixels;
	result.stride = src->width;
	result.width = src->width;
	result.height = src->height;
	return result;
}

Image_view imv_from_subimage (const Image* src, size_t x, size_t y, size_t w, size_t h)
{
	Image_view result;
	result.begin = src->pixels + y * src->width + x;
	result.stride = src->width;
	result.width = w;
	result.height = h;
	return result;
}

Image_view imv_from_subview (const Image_view* src, size_t x, size_t y, size_t w, size_t h)
{
	Image_view result;
	result.begin = src->begin + src->stride * y + x;
	result.stride = src->stride;
	result.width = w;
	result.height = h;
	return result;
}
