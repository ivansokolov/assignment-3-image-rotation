#include "bmp.h"
#include "img.h"
#include "transform.h"
#include "util.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

NORETURN void die_usage (const char* name)
{
	fprintf(stderr,
			"Usage: %s <input> <output> [<mode> <op_param>]\n"
			"Mode defaults to \"rotate ccw\". Possible modes:\n"
			"  rotate <cw|ccw|180>\n"
			"  mirror <x|y|horz|vert>\n"
			"  transp <main|seco>\n", name);
	exit(1);
}

void load_bmp_from_file (Image* image, const char* path)
{
	FILE* stream = fopen(path, "r");
	if (!stream)
		fatal("Failed to open input file \"%s\"", path);
	BMP_load_status status = bmp_load_image(image, stream);
	fclose(stream);
	if (status != BMP_LOAD_OK)
		fatal("Failed to read BMP file: %s", bmp_load_messages[status]);
}

void save_bmp_to_file (const Image* image, const char* path)
{
	FILE* stream = fopen(path, "wb");
	if (!stream)
		fatal("Failed to open output file \"%s\"", path);
	BMP_save_status status = bmp_save_image(image, stream);
	fclose(stream);
	if (status != BMP_SAVE_OK)
		fatal("Failed to write BMP file: %s", bmp_save_messages[status]);
}

uint64_t str_pack8 (const char* str)
{
	uint64_t result = 0;
	for (size_t i = 0; i < 8 && str[i]; i++)
		result = (result << 8) | str[i];
	return result;
}
#define PACK4(A,B,C,D) ((A) | ((B)<<8) | ((C)<<16) | ((D)<<24))
#define PACK8(A,B,C,D,E,F,G,H) \
	((A) | ((B)<<8) | ((uint64_t)(C)<<16) | ((uint64_t)(D)<<24) |\
	((uint64_t)(E)<<32) | ((uint64_t)(F)<<40) | ((uint64_t)(G)<<48) | ((uint64_t)(H)<<56))


int main (int argc, char** argv)
{
	if (argc < 3 || argc == 4)
		die_usage(argv[0]);

	const char* in_path = argv[1];
	const char* out_path = argv[2];

	Image src;
	load_bmp_from_file(&src, in_path);

	enum { ROTATE, MIRROR, TRANSPOSE } operation = ROTATE;
	int op_param = ROTATE_CCW;

	if (argc > 4) {
		if (argc > 5)
			info("Ignoring %i argument(s) past \"%s\"...", argc-5, argv[4]);

		const char* op_str = argv[3];
		const char* param_str = argv[4];

		switch (str_pack8(op_str)) {
		case PACK8('r','o','t','a','t','e', 0, 0):
			operation = ROTATE;
			switch (str_pack8(param_str)) {
			case PACK4('c','w', 0, 0): op_param = ROTATE_CW; break;
			case PACK4('c','c','w',0): op_param = ROTATE_CCW; break;
			case PACK4('1','8','0',0): op_param = ROTATE_180; break;
			default: fatal("Invalid parameter for rotation: %s", param_str);
			}
			break;
		case PACK8('m','i','r','r','o','r', 0, 0):
			operation = MIRROR;
			switch (str_pack8(op_str)) {
			case PACK4('x', 0, 0, 0):
			case PACK4('h','o','r','z'): op_param = MIRROR_ALONG_X; break;
			case PACK4('y', 0, 0, 0):
			case PACK4('v','e','r','t'): op_param = MIRROR_ALONG_Y; break;
			default: fatal("Invalid parameter for mirroring: %s", param_str);
			}
			break;
		case PACK8('t','r','a','n','s','p', 0, 0):
			operation = TRANSPOSE;
			switch (str_pack8(op_str)) {
			case PACK4('m','a','i','n'): op_param = MAIN_DIAGONAL; break;
			case PACK4('s','e','c','o'): op_param = SECONDARY_DIAGONAL; break;
			}
			break;
		default:
			fatal("Invalid operation: %s", op_str);
		}
	}

	Image dest;

	switch (operation) {
	case ROTATE:    img_create_rotated(op_param, &dest, &src); break;
	case MIRROR:    img_create_mirrored(op_param, &dest, &src); break;
	case TRANSPOSE: img_create_transposed(op_param, &dest, &src); break;
	}

	save_bmp_to_file(&dest, out_path);

	img_destroy(&src);
	img_destroy(&dest);
	return 0;
}
