#include "util.h"
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

NORETURN void LIKE_PRINTF(1) fatal (const char* fmt, ...)
{
	fputs("Fatal: ", stderr);
	va_list args;
	va_start(args, fmt);
	vfprintf(stderr, fmt, args); // NOLINT
	va_end(args);
	fputc('\n', stderr);
	exit(1);
}

void LIKE_PRINTF(1) info (const char* fmt, ...)
{
	fputs("Info: ", stderr);
	va_list args;
	va_start(args, fmt);
	vfprintf(stderr, fmt, args); // NOLINT
	va_end(args);
	fputc('\n', stderr);
	fflush(stderr);
}

#ifndef NO_MEMCPY_SUPERSTITION
void* memcpy_but_worse (void* dest, const void* src, size_t amount)
{
	char* cd = dest;
	const char* cs = src;
	// gcc does appear to unroll this a bit, still no match for a real memcpy
	while (amount--)
		*cd++ = *cs++;
	return dest;
}
#endif

void* malloc_or_die (size_t size)
{
	void* result = malloc(size);
	if (!result && size)
		fatal("malloc_or_die(%zu): malloc failed", size);
	return result;
}

void* calloc_or_die (size_t nmemb, size_t size)
{
	void* result = calloc(nmemb, size);
	if (!result && size && nmemb)
		fatal("calloc_or_die(%zu, %zu): calloc failed", nmemb, size);
	return result;
}

void* realloc_or_die (void* ptr, size_t size)
{
	void* result = realloc(ptr, size);
	if (!result && size)
		fatal("realloc_or_die(%p, %zu): realloc failed", ptr, size);
	return result;
}
