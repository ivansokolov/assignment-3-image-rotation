#ifndef TRANSFORM_H
#define TRANSFORM_H

#include "img.h"

/* =============================== Transforming views =============================== */
/*
 * The basic transformation functions are done on image views and not images, as that
 * is more general. For example, you could blit from one portion of the same image
 * to another non-overlapping portion of the same image.
 *
 * In all transforms, `dest` and `src` views must not overlap their pixels
 * (non-overlapping views into the same image is OK)
 */

/* Dimensions must be the same */
void imv_blit (const Image_view* restrict dest, const Image_view* restrict src);

/* Dimensions requiremenet depends on direction... */
typedef enum Rotate_direction {
	ROTATE_CW,  /* ...`src` must fit in `dest` when transposed (width <-> height) */
	ROTATE_CCW, /* ...Same as above */
	ROTATE_180, /* ...`src` must fit in `dest` */
} Rotate_direction;
void imv_rotate (Rotate_direction dir,
		const Image_view* restrict dest,
		const Image_view* restrict src);

/* Dimensions of `src` must fit in `dest` */
typedef enum Mirror_direction {
	MIRROR_ALONG_X,  MIRROR_HORIZONTALLY = MIRROR_ALONG_X,
	MIRROR_ALONG_Y,  MIRROR_VERTICALLY = MIRROR_ALONG_Y,
} Mirror_direction;
void imv_mirror (Mirror_direction dir,
		const Image_view* restrict dest,
		const Image_view* restrict src);

/* Dimensions of `src` must fit in `dest` when transposed (width <-> height) */
typedef enum Transpose_diagonal { MAIN_DIAGONAL, SECONDARY_DIAGONAL } Transpose_diagonal;
void imv_transpose (Transpose_diagonal diag,
		const Image_view* restrict dest,
		const Image_view* restrict src);

/* =========================== Creating transformed images =========================== */
/*
 * Shorthand functions for creating an image with the appropriate size,
 * creating views into both images, and calling the appropriate imv_* transform.
 *
 * `dest` must not already be a valid image. After the call, `dest` becomes a
 * valid image that should be disposed of with `img_destroy`
 */

void img_create_cloned (Image* dest, const Image* src);
void img_create_rotated (Rotate_direction dir, Image* dest, const Image* src);
void img_create_mirrored (Mirror_direction dir, Image* dest, const Image* src);
void img_create_transposed (Transpose_diagonal diag, Image* dest, const Image* src);

#endif /* TRANSFORM_H */
