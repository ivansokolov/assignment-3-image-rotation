#include "bmp.h"

/*
 * In uncompressed BMP, each row of pixels needs padding so that its size aligns by 4
 * However, it appears that there is no such requirement for its offset within the file,
 * defeating the purpose of padding completely.
 *
 * And the tester program requires that we do defeat it by writing image data directly
 * after the following header, whose size is 54 and normally would have required 2 bytes
 * of padding so that each row of pixels would align by 4 bytes. Instead, we insert
 * padding to *misalign* each next row by 2 bytes! Doing the right thing fails tests.
 */

#define BMP_MAGIC 0x4d42

typedef struct BMP_header_packed {
	uint16_t magic;
	uint32_t file_size;
	uint32_t reserved_0;
	uint32_t pixels_start_offset;
	uint32_t dib_size;
	uint32_t width;
	uint32_t height;
	uint16_t planes;
	uint16_t bits_per_pixel;
	uint32_t compression;
	uint32_t image_size;
	uint32_t x_per_meter;
	uint32_t y_per_meter;
	uint32_t colors_used;
	uint32_t colors_important;
} PACKED ALIGNED(2) BMP_header_packed;
static_assert(sizeof(BMP_header_packed) == 54, "Bad packed header struct");

BMP_load_status bmp_load_image (Image* img, FILE* stream)
{
	assert(img && stream);

	unsigned width, height;

	{ // Parse header
		BMP_header_packed header;
		if (fread(&header, sizeof(header), 1, stream) != 1)
			return BMP_LOAD_BAD_STREAM;

		if (header.magic != BMP_MAGIC || header.planes != 1 || header.bits_per_pixel != 24)
			return BMP_LOAD_UNSUPPORTED_HEADER;

		width = header.width;
		height = header.height;

		// skip to pixel stream
		if (fseek(stream, header.pixels_start_offset, SEEK_SET) == -1)
			return BMP_LOAD_BAD_STREAM;
	}

	img_create_uninitialized(img, width, height);
	// From here on, use `goto fail_bad_pixelstream` instead of return

	size_t bytes_per_row = sizeof(RGB_pixel) * width;
	size_t pad = bytes_per_row & 3;

	if (pad == 0) {
		// Without padding, reading can be done in a single chunk
		size_t total_pixels = (size_t) width * height;
		if (fread(img->pixels, sizeof(RGB_pixel), total_pixels, stream) != total_pixels)
			goto fail_bad_pixelstream;
	} else {
		// With padding, we have to read in chunks
		pad = 4-pad;
		for (unsigned row = 0; row < height; row++) {
			if (fread(img->pixels + row * width, sizeof(RGB_pixel), width, stream) != width)
				goto fail_bad_pixelstream;
			// Allow for no padding after the last row
			if (row+1 != height && fseek(stream, (long) pad, SEEK_CUR) == -1)
				goto fail_bad_pixelstream;
		}
	}

	return BMP_LOAD_OK;

fail_bad_pixelstream:
	img_destroy(img);
	return BMP_LOAD_BAD_PIXELSTREAM;
}

BMP_save_status bmp_save_view (const Image_view* imv, FILE* stream)
{
	assert(imv && stream);

	size_t row_pitch = imv->width * sizeof(RGB_pixel);
	size_t row_pad = (4 - (row_pitch & 3)) & 3;

	size_t img_size = (imv->height-1) * (row_pitch + row_pad) + row_pitch;

	BMP_header_packed header = {
		.magic = BMP_MAGIC,
		.file_size = sizeof(BMP_header_packed) + img_size,
		.reserved_0 = 0,
		.pixels_start_offset = sizeof(BMP_header_packed),
		.dib_size = sizeof(BMP_header_packed)-offsetof(BMP_header_packed, dib_size),
		.width = imv->width,
		.height = imv->height,
		.planes = 1,
		.bits_per_pixel = 24,
		.compression = 0, // uncompressed RGB
		.image_size = img_size,
		.x_per_meter = 0,
		.y_per_meter = 0,
		.colors_used = 0,
		.colors_important = 0
	};
	if (fwrite(&header, sizeof(header), 1, stream) != 1)
		return BMP_SAVE_WRITE_FAILED;

	if (row_pad == 0) {
		// Without padding, we can write the image out in a single pass
		unsigned total_pixels = imv->width * imv->height;
		if (fwrite(imv->begin, sizeof(RGB_pixel), total_pixels, stream) != total_pixels)
			return BMP_SAVE_WRITE_FAILED;
	} else {
		// Else, we will pad with at most 3 zeros each time
		char pad_buffer[3] = { 0 };
		unsigned w = imv->width;
		for (unsigned row = 0; row < imv->height; row++) {
			if (fwrite(imv->begin + row * w, sizeof(RGB_pixel), w, stream) != w)
				return BMP_SAVE_WRITE_FAILED;
			if (row+1 != imv->height && fwrite(pad_buffer, row_pad, 1, stream) != 1)
				return BMP_SAVE_WRITE_FAILED;
		}
	}

	return BMP_SAVE_OK;
}

BMP_save_status bmp_save_image (const Image* img, FILE* stream)
{

	Image_view imv = imv_from_image(img);
	return bmp_save_view(&imv, stream);
}
