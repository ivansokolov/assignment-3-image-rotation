#ifndef IMG_H
#define IMG_H

#include "util.h"
#include <stddef.h>
#include <stdint.h>

typedef union RGB_pixel {
	uint8_t rgb[3];
	struct { uint8_t r, g, b; } PACKED;
} PACKED RGB_pixel;
static_assert(sizeof(RGB_pixel) == 3 && ALIGNOF(RGB_pixel) == 1
		&& offsetof(RGB_pixel, r) == offsetof(RGB_pixel, rgb[0])
		&& offsetof(RGB_pixel, g) == offsetof(RGB_pixel, rgb[1])
		&& offsetof(RGB_pixel, b) == offsetof(RGB_pixel, rgb[2]),
		"Bad RGB_pixel layout");

/* ====================================== Image ====================================== */
/*
 * The Image struct represents the ownership over the pixel data,
 * can be managed with `img_create_*` and `img_destroy`
 */
typedef struct Image {
	size_t width;  /* Both in pixels */
	size_t height;
	RGB_pixel* pixels;
} Image;

/* `img` must not be an already valid image */
void img_create_zeroed (Image* img, size_t width, size_t height);
void img_create_uninitialized (Image* img, size_t width, size_t height);

/* `img` must be a valid image */
void img_destroy (Image* img);

/* =================================== Image views =================================== */
/*
 * An image view can refer to a rectangular portion of an image, without owning any of it.
 * It is the canonical primitive on which all image operations are built.
 * Arbitrarily many image views can refer to the image data of any image.
 */
typedef struct Image_view {
	RGB_pixel* begin;
	size_t stride;  /* All in pixels */
	size_t width;
	size_t height;
} Image_view;

#define IMGVIEW_PIXEL_OFFS(VIEW,X,Y) ((Y)*((VIEW)->stride)+(X))

Image_view imv_from_image (const Image* src);
Image_view imv_from_subimage (const Image* src, size_t x, size_t y, size_t w, size_t h);
Image_view imv_from_subview (const Image_view* src, size_t x, size_t y, size_t w, size_t h);

#endif /* IMG_H */
